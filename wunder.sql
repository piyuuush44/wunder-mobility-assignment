-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2018 at 05:00 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wunder`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` bigint(20) NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_no` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iban` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `registration_process` enum('1','2','3') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `telephone`, `street`, `house_no`, `zip_no`, `city`, `account_owner`, `iban`, `paymentDataId`, `created_at`, `updated_at`, `registration_process`) VALUES
(1, 'Neha', 'nehra', 7060640847, 'kadrabad', '234', 201201, 'modinagar', 'ompal nehra', '123456', 'a0548b04ad64a0902fc3a6702616d5d736301b7c504096a746e7b329610551cb54c7e46a0d8e6033042db6b924461380', '2018-12-08 11:12:31', '2018-12-08 11:18:03', '3'),
(2, 'sumit', 'singh', 8979545883, 'new defence colony', '271', 201206, 'GHAZIABAD', 'sumit prasad', 'iban234', '4540c92c638f02ba5945ae797c6cad2a2b6474d74b5361a99fb381bddc0a46fc19b45694be830aefc68e484d19471f0a', '2018-12-08 11:21:27', '2018-12-08 11:21:50', '3'),
(3, 'piyush', 'singh', 8979545884, '269 ,GALI NO 9,NEW DEFENCE COLONY,MURADNAGAR,  BEHIND POLICE, station', '98', 201206, 'Select', 'piyush singh', 'lskdgjlskgj', '448a844085639788cb2a88e8ac149f409b00a38d2e3f58c4b698b8db2d424d845bec3ca580a72b6775ab443130a2e31a', '2018-12-08 12:02:32', '2018-12-08 12:03:08', '3'),
(4, 'piyush', 'singh', 897954884, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-09 01:45:47', '2018-12-09 01:45:47', '1'),
(6, 'piyush', 'singh', 12345, '269 ,GALI NO 9,NEW DEFENCE COLONY,MURADNAGAR,  BEHIND POLICE, station', '23234', 201206, 'Select', 'piyush singh', 'lskdgjlskgj', '2ca3bff1ad85f748717e2ceb3a6489e3049eefd880909b1f61ef410a64bb6dc3c8d62ef81e9f4f4f7cc1bbf682159c4a', '2018-12-09 02:05:11', '2018-12-09 02:05:27', '1'),
(7, 'piyush', 'singh', 897545884, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-09 02:13:30', '2018-12-09 02:13:30', '1'),
(8, 'piyush', 'dskjfs', 89884, 'pakistan', '2425352', 232, 'mayalaldkj', 'piyush sin', 'slkgs', '5bf230901a978a4bec369876d92bdf72cc741a95b23ef3e3058582461c3ae6fa2bb5e72effaf28d45e3452aa9611d985', '2018-12-09 03:35:50', '2018-12-09 07:13:49', '3'),
(9, 'piyush', 'singh', 8979675885, 'lkjsdfj', '2332', 201203, 'llfshflks', 'piyush hu', '12345', 'cf798a8bac5013efd31f90452bd2a22cb23b6a22e04475c2724a6cb686e4fb84c571e80745cc8b9d7089845303b96fd1', '2018-12-09 07:23:25', '2018-12-09 07:23:47', '3'),
(10, 'details', 'of', 390433444, 'na h kuch bhi', '242', 201202, 'na btare', 'mahashay', '456', '7da169c0d0aa8d1b2a6fd7e44974019c8a71da4dc659f7b2fb21f96460800404007954cdb81f526a80a476f04c507ea1', '2018-12-09 07:34:16', '2018-12-09 07:37:38', '3');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_12_07_181308_customers', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_telephone_uindex` (`telephone`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
