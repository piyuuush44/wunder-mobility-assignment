There is screenshot of db structure in DBScreenshot.png file and structure is exported into wunder.sql file

In the software Database, 
Primary key is - id
Unique key is � telephone
There is a registration_process column with values 1,2,3 which is helpful in determining the registration status of the user in the system. Where 1 is available when first view details are inserted into database , 2 is available when address details are updated in the second view , 3 is available when payment details are also updated for the customer. 


Q - Describe possible performance optimizations for your Code.
A � There should be better middleware logic for the purpose of allowing the user to re enter his details if he is left in between. 
Q - Which things could be done better, than you�ve done it?
A - There should be a user session for each user to login into the system first and then process the further tasks.  I have used Laravel 5.7 for the backend architecture , which is having its internal auth for login and registration process but I haven�t used it.


