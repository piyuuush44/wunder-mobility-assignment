<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('register', 'RegisterController@view')->name('register')->middleware('Registration');;

Route::post('firstdetails', 'RegisterController@firstdetails')->name('firstdetails');

Route::post('address', 'RegisterController@address')->name('address');

Route::post('payment', 'RegisterController@payment')->name('payment');

Route::get('firstRedirect', 'RegisterController@firstRedirect')->name('firstRedirect');

Route::get('secondRedirect', 'RegisterController@secondRedirect')->name('secondRedirect');
//Route::view('firstRedirect', 'address', $telephone);

//Route::view('secondRedirect', 'payment', ['telephone' => $telephone]);
