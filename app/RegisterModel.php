<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class RegisterModel extends Model
{

    protected $table = 'customers';
    protected $fillable = ['street', 'house_no', 'zip_no', 'city', 'account_owner', 'iban', 'paymentDataId', 'registration_process'];

    public static function check_last_entry()
    {
        //checking the status of last user
        $check = RegisterModel::orderBy('id', 'desc')
            ->get()
            ->toArray();
        return isset($check[0]['registration_process']) ? ['value' => $check[0]['registration_process'], 'telephone' => $check[0]['telephone']] : NULL;
    }

    public static function validate($telephone)
    {
        //validating if phone number is already present in the system
        $check = RegisterModel::where('telephone', $telephone)
            ->count();

        if ($check > 0) {
            return false;
        } else {
            return true;
        }
    }


    public static function first_details($request)
    {
        //inserting of customer data
        $validate = RegisterModel::validate($request->input('telephone'));
        if ($validate == true) {
            $customer = new RegisterModel();
            $customer->first_name = $request->input('first_name');
            $customer->last_name = $request->input('last_name');
            $customer->telephone = $request->input('telephone');
            $customer->registration_process = 1;
            if ($customer->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            return 1;
        }
    }

    public static function address($request)
    {
        //updating address related data
        $customers = RegisterModel::updateOrCreate(
            ['telephone' => $request->input('telephone')],
            ['registration_process' => 2,
                'street' => $request->input('street'),
                'house_no' => $request->input('house_no'),
                'zip_no' => $request->input('zip_no'),
                'city' => $request->input('city')]);
        if ($customers->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function finalCall($request)
    {
        //initiating a curl request to payment gateway of wunder
        $endpoint = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
        $client = new \GuzzleHttp\Client();
        $body = ['customerId' => $request->input('telephone'),
            'iban' => $request->input('iban'),
            'owner' => $request->input('account_owner')
        ];

        Log::debug('curl body ' . var_export($body, true));
        $response = $client->request('POST', $endpoint, ['json' => $body]);

        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getBody(), true);

        //saving the data obtained into database
        $customers = RegisterModel::updateOrCreate(
            ['telephone' => $request->input('telephone')],
            ['paymentDataId' => $content['paymentDataId'],
                'registration_process' => 3]);
        if ($customers->save()) {
            return $content['paymentDataId'];
        } else {
            return false;
        }

    }

    public static function payment($request)
    {
        //updating payment related data for the customer
        $customers = RegisterModel::updateOrCreate(
            ['telephone' => $request->input('telephone')],
            ['registration_process' => 2,
                'account_owner' => $request->input('account_owner'),
                'iban' => $request->input('iban')]);
        if ($customers->save()) {
            return true;
        } else {
            return false;
        }
    }


}
