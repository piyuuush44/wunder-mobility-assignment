<?php

namespace App\Http\Controllers;

use App\RegisterModel;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function view()
    {
        //returning default view
        return view('register');
    }

    public function firstRedirect()
    {
        //controller for redirecting for old users
        $check = RegisterModel::check_last_entry();
        return view('address', ['telephone' => $check['telephone']]);
    }

    public function secondRedirect()
    {
        //controller for redirecting for old users
        $check = RegisterModel::check_last_entry();
        return view('payment', ['telephone' => $check['telephone']]);
    }

    public function firstdetails(Request $request)
    {
        //controller for insertion of data into database
        $data = RegisterModel::first_details($request);
        if ($data === true) {
            return view('address', ['telephone' => $request->input('telephone')]);
        } elseif ($data === 1) {
            return view('error', ['error' => 'Please enter a unique telephone no']);
        } else {
            return view('error', ['error' => '404 not found']);
        }
    }

    public function address(Request $request)
    {
        //controller for updating address fields
        $data = RegisterModel::address($request);
        if ($data == true) {
            return view('payment', ['telephone' => $request->input('telephone')]);
        } else {
            return view('error', ['error' => '404 not found']);
        }
    }

    public function payment(Request $request)
    {
        //controller for updating payment fields
        $data = RegisterModel::payment($request);
        if ($data == true) {
            $finalCall = RegisterModel::finalCall($request);
            if ($finalCall !== false) {
                return view('success', ['paymentDataId' => $finalCall]);
            } else {
                return view('error', ['error' => '404 not found']);
            }
        } else {
            return view('error', ['error' => '404 not found']);
        }
    }
}
