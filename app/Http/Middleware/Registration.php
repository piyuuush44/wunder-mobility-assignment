<?php

namespace App\Http\Middleware;

use App\RegisterModel;
use Closure;

class Registration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //middleware to check the last entry and return to the specific view
        $check = RegisterModel::check_last_entry();
        if ($check['value'] == 1) {
            return redirect()->route('firstRedirect')->with('telephone', $check['telephone']);
        } elseif ($check['value'] == 2) {
            return redirect()->route('secondRedirect', ['telephone' => $check['telephone']]);
        } else {
            return $next($request);
        }
    }
}
