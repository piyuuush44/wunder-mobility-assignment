<html>
<head>
    <!-- Website CSS style -->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">

    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

    </style>

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="jumbotron text-xs-center">

        <h1 align="center">Error!!!!!</h1>
        <p align="center"><strong>{{$error}}</strong></p>
        <div class="col-md-6 offset-md-4">
            <p class="lead" align="center">
                <a class="btn btn-primary btn-sm" href="{{route('register')}}" role="button">Register Again ?</a>
            </p>

        </div>
    </div>

</div>

</body>
</html>
