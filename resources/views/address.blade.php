<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Address</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:20,60" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">--}}

<!-- Website CSS style -->
{{--<link rel="stylesheet" type="text/css" href="assets/css/main.css">--}}

<!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>


    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }
        .control-label{
            margin-left: 530px;
        }
        .input-group {
            width: 55%;
            margin-left: 250px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="container">
        <div class="row main">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h3 class="title">Enter your details</h3>
                    <hr/>
                </div>
            </div>
            <div class="main-login main-center">
                <form class="form-horizontal" method="post" action="{{ route('address') }}">

                    <div class="form-group">
                        @csrf
                        <label for="house_no" class="cols-sm-2 control-label">House No.</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="number" class="form-control" name="house_no"
                                       placeholder="Enter your House no" required/>
                                <input type="hidden" id="telephone" name="telephone" value="{{$telephone}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="street" class="cols-sm-2 control-label">Street Address</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="street"
                                       placeholder="Enter your Street address" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="City" class="cols-sm-2 control-label">City</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="city"
                                       placeholder="Enter your First Name" required/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="zip_no" class="cols-sm-2 control-label">Zip No.</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="number" class="form-control" name="zip_no"
                                       placeholder="Enter your Telephone Number" required/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4" align="right">
                            <button type="submit" class="btn btn-primary"> Next</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>
</body>
</html>
